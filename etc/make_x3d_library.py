#!/usr/bin/python3
#
# This script creates a .blend library file using a directory filled
# with X3D models.
#

import os
import sys
import glob
import bpy
import re

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]
def run():
    print(sys.argv)
    args = sys.argv[sys.argv.index('--')+1:]
    if not len(args) == 1:
        raise Exception("Need 1 argument which is the source directory "
                        "of X3D files.")
    source_dir = os.path.abspath(args[0])

    # scan for X3D files
    print("Scanning %s for X3D files..." % source_dir)
    files = glob.glob(source_dir + "/*.[xX]3[dD]")
    if files:
        print("Found %d files." % len(files))
    else:
        raise Exception("Couldn't find any files named *.x3d in source directory.")

    files = sorted(files, key=natural_sort_key)

    # create case and pin materials
    pinsmat = bpy.data.materials.new("pins")
    pinsmat.diffuse_color = (0.9, 0.9, 0.9)
    casemat = bpy.data.materials.new("case")
    casemat.diffuse_color = (0.05, 0.05, 0.05)

    # import
    pos_y = 0
    for x3d_file in files:
        bpy.ops.import_scene.x3d(filepath=x3d_file, axis_forward='Y', axis_up='Z')

        case = bpy.context.selected_objects[1]
        pins = bpy.context.selected_objects[0]

        case.data.materials[0] = casemat
        pins.data.materials[0] = pinsmat

        bpy.context.scene.objects.active = bpy.context.selected_objects[0]
        bpy.ops.object.join()
        name = os.path.basename(x3d_file)
        name = os.path.splitext(name)[0]
        obj = bpy.context.active_object
        obj.name = name
        obj.location.y = pos_y
        pos_y += 3

if __name__ == "__main__":
    run()
