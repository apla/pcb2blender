# Blender specific utilities for pcb2blender
# mostly drawing functions
# REMEMBER: in Blender units are 'cm'
# 1 Blender unit = 1 cm
# but other units (anything coming from outside) are 'mm'
# so before drawing something scale it to 1/10

import bpy,os
from utils import Point
from math import radians,pi

def clearScene():
    for obj in bpy.data.objects:
        if obj.type == "MESH":
            obj.select = True

    bpy.ops.object.delete()

    for mesh in bpy.data.meshes:
        bpy.data.meshes.remove(mesh)

def drawBoard(points):
    # first of all, scale coordinates so 1cm=1 blender unit
    points = [p*0.1 for p in points]

    del points[-1]

    t = 0.2 # board thickness in cm
    N = len(points) # number of points on path

    topVertices = [(p.x,p.y,t/2) for p in points]
    bottomVertices = [(p.x,p.y,-t/2) for p in points]

    vertices = topVertices+bottomVertices

    topFace = tuple(range(0,N))
    bottomFace = tuple(range(N,2*N))

    sideFaces = [(i,i+1,N+i+1,N+i) for i in range(0,N-1)]
    sideFaces.append((N-1,0,N,2*N-1))

    faces = [topFace,bottomFace]+sideFaces

    mesh = bpy.data.meshes.new('BoardMesh')
    obj = bpy.data.objects.new('Board',mesh)
    bpy.context.scene.objects.link(obj)

    mesh.from_pydata(vertices,[],faces)
    mesh.update(calc_edges=True)
    mesh.calc_normals()
    mesh.update()

    # make normals consistent!!
    bpy.context.scene.objects.active = obj
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.normals_make_consistent()
    bpy.ops.object.mode_set(mode='OBJECT')

def extractHoles(holes):
    # first scale coordinates/sizes 1cm=1 blender unit
    for h in holes:
        h.pos *= 0.1
        h.drill *= 0.1

    t = 0.2 # board thickness in cm

    # crate cylinders representing holes
    bpy.ops.object.add(type='MESH',location=(0,0,0),enter_editmode=True)
    bpy.context.edit_object.name = "holes"

    add_cylinder = bpy.ops.mesh.primitive_cylinder_add

    for hole in holes:
        loc = (hole.pos.x,hole.pos.y,0)
        # TODO: make this scalar selectable
        numverts = round(hole.drill*100)
        if numverts < 16 : numverts = 16
        add_cylinder(radius=hole.drill/2,depth=t*1.1,
                     location=loc,vertices=numverts)

    # subtract holes from board object
    bpy.ops.object.mode_set(mode='OBJECT')
    board = bpy.data.objects["Board"]
    bpy.context.scene.objects.active = board
    bpy.ops.object.modifier_add(type='BOOLEAN')
    modifier = board.modifiers[0]
    modifier.object = bpy.data.objects["holes"]
    modifier.operation = 'DIFFERENCE'
    bpy.ops.object.modifier_apply(modifier="Boolean")

    bpy.context.scene.objects.active = bpy.data.objects["holes"]
    bpy.ops.object.delete()

def createTexMat(name,side):
    # load image and crate texture
    fname = os.path.abspath(side+"tex.png")
    img = bpy.data.images.load(fname)
    tex = bpy.data.textures.new(name+"Tex",type='IMAGE')
    tex.image = img

    # load bump image
    fname2 = os.path.abspath(side+"bump.png")
    imgb = bpy.data.images.load(fname2)
    texb = bpy.data.textures.new(name+"BTex",type='IMAGE')
    texb.image = imgb

    #crate Material and set texture
    mat = bpy.data.materials.new(name+"Mat")

    mtex = mat.texture_slots.add()
    mtex.texture = tex
    mtex.texture_coords = 'ORCO'
    mtex.use_map_color_diffuse = True

    bmtex = mat.texture_slots.add()
    bmtex.texture = texb
    bmtex.texture_coords = 'ORCO'
    bmtex.use_map_color_diffuse = False
    bmtex.use_map_normal = True
    bmtex.normal_factor = -0.2

    return mat

def applyTextures():
    """Applies textures on board if they exists. These files should be
    present in the current working directory:
    - toptex.png
    - bottomtex.png
    - topbump.png
    - bottombump.png
    If one of these files is missing, function will return False.
    """
    # check existence of texture files
    isfile = os.path.isfile
    if not (isfile('toptex.png') or
            isfile('bottomtex.png') or
            isfile('topbump.png') or
            isfile('bottoumbump.png')):
        return False

    t = 0.2 # board thickness in cm

    mboard = bpy.data.materials.new("BoardMat")
    mboard.diffuse_color = (1,0,0)  # red
    mtop = createTexMat("BoardTop","top")
    mtop.diffuse_color = (1,0,0)
    mbot = createTexMat("BoardBot","bottom")
    mbot.diffuse_color = (1,0,0)

    board = bpy.data.objects["Board"]
    mesh = board.data
    mesh.materials.append(mboard)
    mesh.materials.append(mtop)
    mesh.materials.append(mbot)

    topface = mesh.polygons[0]
    botface = mesh.polygons[1]

    topface.material_index = 1
    botface.material_index = 2

    return True

# file should be absolute path!
def placeModel(name, file, model, x, y, scale=1.0, rot=0.0, mir=False):

    t = 0.2     # board thickness in cm

    # import model from a STL or blender file
    if file[-3:] == 'stl':
        bpy.ops.import_mesh.stl(filepath = file)
    else:
        if bpy.app.version[1] >= 72:
            append = bpy.ops.wm.append
        else:
            append = bpy.ops.wm.link_append
        append(directory=file+'/Object/', filename=model, link=False,
               autoselect=True)

    # place, scale and rotate the model
    obj = bpy.context.selected_objects[0]

    obj.name = name

    obj.location.x = x/10
    obj.location.y = y/10
    obj.location.z = t/2 if not mir else -t/2

    obj.scale *= scale

    obj.rotation_euler = (0,0,rot) if not mir else (0,pi,-rot)
