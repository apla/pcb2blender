# Eagle Specific functions and other stuff for pcb2blender

import os.path
import glob
import shutil
from math import radians
from utils import Line
import subprocess as sp
import glob

def getEagle():
    eagle = shutil.which('eagle')
    if eagle==None:
        path = glob.glob('/opt/eagle*/bin/eagle')
        # TODO: maybe add eagle executable selection
        if len(path)>0:
            path = path[0]
            eagle = shutil.which(path)

    if eagle==None:
        raise FileNotFoundError(1,"Can't find Eagle executable path")
    else:
        return eagle

"""Returns list of Lines from <wire>s"""
def getWireListFromET(wires):
    wlist = []

    for wire in wires:
        w = Line(float(wire.attrib['x1']),
                 float(wire.attrib['y1']),
                 float(wire.attrib['x2']),
                 float(wire.attrib['y2']))
        c = wire.get('curve')
        if c != None:
            w.curve = radians(float(c))
        wlist.append(w)

    return wlist

def makeTextures(file, dpi):
    eaglepath = getEagle()

    boardFile = os.path.abspath(file)

    pcbcolor = "#A30008"
    pcbcolorc = "#FB000D"
    tincolor = "#C3C3C3"

    dpis = str(dpi)

    exportsc =  "SET PALETTE WHITE;" \
                "SET PAD_NAMES OFF;" \
                "SET FILL_LAYER 29 Solid;" \
                "SET FILL_LAYER 30 Solid;" \
                "WINDOW;" \
                "SET CONFIRM YES;" \
                "DISPLAY none 20;" \
                "EXPORT IMAGE dimension.png MONOCHROME "+dpis+";" \
                "DISPLAY none 1 17 18;" \
                "RATSNEST;" \
                "EXPORT IMAGE topcopper.png MONOCHROME "+dpis+";" \
                "DISPLAY none 16 17 18;" \
                "EXPORT IMAGE bottomcopper.png MONOCHROME "+dpis+";" \
                "DISPLAY none 21 25 27;" \
                "EXPORT IMAGE topsilk.png MONOCHROME "+dpis+";" \
                "DISPLAY none 22 26 28;" \
                "EXPORT IMAGE bottomsilk.png MONOCHROME "+dpis+";" \
                "DISPLAY none 29;" \
                "EXPORT IMAGE topstop.png MONOCHROME "+dpis+";" \
                "DISPLAY none 30;" \
                "EXPORT IMAGE bottomstop.png MONOCHROME "+dpis+";" \
                "quit;"

    # call eagle to produce image files
    sp.call([eaglepath,"-C "+exportsc,boardFile])

    # process image files with magic to produce textures

    if os.name == 'nt':
        convert_fullpath = shutil.which('convert')
        if convert_fullpath == None:
            raise FileNotFoundError(1,"Can't find imagemagic executable."
                " Please make sure imagemagic installation directory is "
                "at the beginning of your PATH variable!")

    def parse_command(command):
        args = command.split()
        if os.name == 'nt':
            if args[0] == 'convert':
                args[0] = convert_fullpath
        return args

    imcommand = "convert -trim -format %wx%h%O dimension.png info:"

    cropsize = sp.check_output(parse_command(imcommand),universal_newlines=True)
    cropsize = cropsize.strip()

    imcommands = ["convert topsilk.png -crop "+cropsize+" +repage topsilk.png",
    "convert bottomsilk.png -crop "+cropsize+" +repage bottomsilk.png",
    "convert topcopper.png -crop "+cropsize+" +repage topcopper.png",
    "convert bottomcopper.png -crop "+cropsize+" +repage bottomcopper.png",
    "convert topstop.png -crop "+cropsize+" +repage topstop.png",
    "convert bottomstop.png -crop "+cropsize+" +repage bottomstop.png",
    "cp topcopper.png topbump.png",
    "cp bottomcopper.png bottombump.png",
    "convert topcopper.png -fill "+pcbcolor+" -opaque white topcopper.png",
    "convert topcopper.png -fill "+pcbcolorc+" -opaque black topcopper.png",
    "convert bottomcopper.png -fill "+pcbcolor+" -opaque white bottomcopper.png",
    "convert bottomcopper.png -fill "+pcbcolorc+" -opaque black bottomcopper.png",
    "convert topstop.png -transparent white topstop.png",
    "convert topstop.png -fill "+tincolor+" -opaque black topstop.png",
    "convert bottomstop.png -transparent white bottomstop.png",
    "convert bottomstop.png -fill "+tincolor+" -opaque black bottomstop.png",
    "convert topsilk.png -transparent white topsilk.png",
    "convert topsilk.png -fill white -opaque black topsilk.png",
    "convert bottomsilk.png -transparent white bottomsilk.png",
    "convert bottomsilk.png -fill white -opaque black bottomsilk.png",
    "convert topcopper.png topsilk.png topstop.png -flatten toptex.png",
    "convert bottomcopper.png bottomsilk.png bottomstop.png"
        " -flatten bottomtex.png",
    "convert topbump.png -blur 1x4 topbump.png",
    "convert bottombump.png -blur 1x4 bottombump.png"]

    for cmd in imcommands:
        try:
            args = parse_command(cmd)
            # NOTE: under windows there is no `copy.exe`, so we are using python
            if args[0] == 'cp':
                shutil.copy(args[1], args[2])
            else:
                sp.call(args)
        except:
            print("Error while running command: " + cmd)
            raise
